import logging
import os


def logs(current_path, log_name):
    logger = logging.getLogger()
    log_file = os.path.join(current_path, log_name)
    handler = logging.FileHandler(log_file)
    formatter = logging.Formatter('[%(asctime)s %(levelname)s] %(message)s')
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    logger.setLevel(logging.INFO)

    return logger