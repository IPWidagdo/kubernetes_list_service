# Get Kubernetes Service List

## Background

The purpose of this script is to fulfill the task that was given by Xtremax as a recruitment test.

## How I do it?
Before I made the script I need to create an environment and condition so that the result can match with the requirements.

Below is the simplified step by step on how I do it:
```
1. Create a script which produce an API, for this occasion I the script called bmi_sayur.py, the script produce the 
    calculation of Body Mass Index using height (in cm) and weight (in kg) to determined wether you are underweight, 
    normal, or overweight. This script using Bottle as a framework
2. Created an AWS account, and made an instance using AWS Linux
3. Installed git, python, docker, kubeadm, kubelet, and kubectl
4. Made and image and run it as a Docker container
5. Run it as a Kubernetes service
6. Create the main.py script in my local PC
7. Push the script to a git repository
8. Clone the repository to the AWS instance to be tested 
9. Done
```

## How to run the script
```
1. Make sure you have Python 3 in your PC
2. Run command:
    python main.py
3. The result is located in the same folder with main.py with the name service_list.csv
```

## How to open the file if you are using Excel
```
1. Open Excel
2. Click Blank Workbook
3. Go to Data
4. Click From Text/CSV
5. Make sure the delimiter is "Comma"
6. Click Load
```
