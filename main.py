import log_functions
import os
import subprocess
import csv


def send_command():
    # =============================================================================================
    # Send command and retrieve output
    res = subprocess.run(['kubectl', 'get', 'service', '--all-namespaces'], stdout=subprocess.PIPE)
    string_output = res.stdout.decode("utf-8")
    split_by_line = string_output.splitlines()
    # =============================================================================================

    # =============================================================================================
    # Turn output into string
    output = list()
    for splits in split_by_line:
        f = "|".join(splits.split())
        output.append(f)
    # =============================================================================================

    return output


def save_as_csv(filename, data):

    # =============================================================================================
    # Create CSV file, and put the output value from the previous command
    with open(filename, 'w', encoding='UTF8', newline='') as f:
        writer = csv.writer(f)

        for k in data:
            writer.writerow(k.split('|'))
    # =============================================================================================


if __name__ == "__main__":
    # Initialize current path as a reference path so that the log file will be in the same page as main.py
    current_path = os.path.dirname(os.path.realpath(__file__))

    # Calling log function and the parameters needed, will create log.log if it does not exist.
    log_path = os.path.join(current_path)
    log_name = 'log.log'
    logs = log_functions.logs(log_path, log_name)

    # Send command to Linux Terminal
    try:
        output_data = send_command()
        logs.info("Successfully send command to terminal.")
    except Exception as e:
        logs.error("Failed to send command.")
        logs.error(e)

    # Save the output to CSV file
    try:
        if len(output_data) > 0:
            save_as_csv(os.path.join('service_list.csv'), output_data)
            logs.info("Successfully save output as CSV file.")
        else:
            logs.error("Failed to save CSV file, because the output is empty.")
    except Exception as x:
        logs.error("Failed to save CSV file.")
        logs.error(x)
    # cdk = ['NAMESPACE|NAME|TYPE|CLUSTER-IP|EXTERNAL-IP|PORT(S)|AGE', 'default|kubernetes|ClusterIP|10.96.0.1|<none>|443/TCP|68m', 'kube-system|kube-dns|ClusterIP|10.96.0.10|<none>|53/UDP,53/TCP,9153/TCP|68m']

